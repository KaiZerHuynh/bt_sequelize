import express from "express";
import likeRoute from "./likeRoute.js";
import rateRoute from "./rateRoute.js";
import orderRoute from "./orderRoute.js";

const rootRoute = express.Router();

rootRoute.use("/restaurants", likeRoute);
rootRoute.use("/rates", rateRoute);
rootRoute.use("/orders", orderRoute);

export default rootRoute;
