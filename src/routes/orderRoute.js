import express from "express";
import { addOrders, getOrders } from "../controllers/orderController.js";

const orderRoute = express.Router();

orderRoute.post("/create-orders", addOrders);
orderRoute.get("/get-orders", getOrders);

export default orderRoute;
