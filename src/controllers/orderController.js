import initModels from "../models/init-models.js";
import sequelize from "../models/connect.js";

const model = initModels(sequelize);

const addOrders = async (req, res) => {
  const { code, arr_sub_id, amount, user_id, food_id } = req.query;
  let createOrder = await model.orders.create({
    code,
    arr_sub_id,
    amount,
    user_id,
    food_id,
  });
  res.send(createOrder);
};

const getOrders = async (req, res) => {
  let orders = await model.orders.findAll();
  res.send(orders);
};

export { addOrders, getOrders };
