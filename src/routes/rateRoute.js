import express from "express";
import { addRates, getRates } from "../controllers/rateController.js";

const rateRoute = express.Router();

rateRoute.post("/create-rates", addRates);
rateRoute.get("/get-rates", getRates);

export default rateRoute;
