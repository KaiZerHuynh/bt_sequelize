import initModels from "../models/init-models.js";
import sequelize from "../models/connect.js";

const model = initModels(sequelize);

const addRates = async (req, res) => {
  const { res_id, user_id, data_rata, amount } = req.query;
  let createRate = await model.rate_res.create({
    res_id,
    user_id,
    data_rata,
    amount,
  });
  res.send(createRate);
};

const getRates = async (req, res) => {
  const ratings = await model.rate_res.findAll();
  res.send(ratings);
};

export { addRates, getRates };
