import express from "express";
import {
  getLikes,
  deleteLikes,
  createLikes,
} from "../controllers/likeController.js";

const likeRoute = express.Router();

// Xử lý like nhà hàng
likeRoute.post("/create-likes", createLikes);
likeRoute.delete("/delete-likes/:id_like", deleteLikes);
likeRoute.get("/get-likes", getLikes);

export default likeRoute;
