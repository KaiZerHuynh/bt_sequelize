import express from "express";
import rootRoute from "./routes/rootRoute.js";
// import mysql from "mysql2";
// const connect = mysql.createConnection({
//   host: "localhost",
//   user: "root",
//   password: "1234",
//   port: "3306",
//   database: "db_app_food",
// });

const app = express();
app.use(express.json());

app.listen(8080);

app.use(rootRoute);
