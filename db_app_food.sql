/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `food_types` (
  `type_id` int NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `foods` (
  `food_id` int NOT NULL AUTO_INCREMENT,
  `food_name` varchar(50) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `descs` varchar(100) DEFAULT NULL,
  `type_id` int DEFAULT NULL,
  PRIMARY KEY (`food_id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `foods_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `food_types` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `like_res` (
  `id_like` int NOT NULL AUTO_INCREMENT,
  `data_like` datetime DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `res_id` int DEFAULT NULL,
  PRIMARY KEY (`id_like`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orders` (
  `id_orders` int NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `arr_sub_id` varchar(100) DEFAULT NULL,
  `amount` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `food_id` int DEFAULT NULL,
  PRIMARY KEY (`id_orders`),
  KEY `user_id` (`user_id`),
  KEY `food_id` (`food_id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`food_id`) REFERENCES `foods` (`food_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `rate_res` (
  `id_rate` int NOT NULL AUTO_INCREMENT,
  `amount` int DEFAULT NULL,
  `data_rate` datetime DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `res_id` int DEFAULT NULL,
  PRIMARY KEY (`id_rate`),
  KEY `user_id` (`user_id`),
  KEY `res_id` (`res_id`),
  CONSTRAINT `rate_res_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `rate_res_ibfk_2` FOREIGN KEY (`res_id`) REFERENCES `restaurants` (`res_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `restaurants` (
  `res_id` int NOT NULL AUTO_INCREMENT,
  `res_name` varchar(50) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `descs` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`res_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sub_foods` (
  `sub_id` int NOT NULL AUTO_INCREMENT,
  `sub_name` varchar(50) DEFAULT NULL,
  `sub_price` float DEFAULT NULL,
  `food_id` int DEFAULT NULL,
  PRIMARY KEY (`sub_id`),
  KEY `food_id` (`food_id`),
  CONSTRAINT `sub_foods_ibfk_1` FOREIGN KEY (`food_id`) REFERENCES `foods` (`food_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `fullname` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `food_types` (`type_id`, `type_name`) VALUES
(1, 'Cơm');
INSERT INTO `food_types` (`type_id`, `type_name`) VALUES
(2, 'Canh');
INSERT INTO `food_types` (`type_id`, `type_name`) VALUES
(3, 'Đồ Mặn');
INSERT INTO `food_types` (`type_id`, `type_name`) VALUES
(4, 'Cơm'),
(5, 'Canh'),
(6, 'Đồ Mặn'),
(7, 'Tráng Miệng');

INSERT INTO `foods` (`food_id`, `food_name`, `image`, `price`, `descs`, `type_id`) VALUES
(1, 'Cơm gà', 'abc', 100, 'ngon', 1);
INSERT INTO `foods` (`food_id`, `food_name`, `image`, `price`, `descs`, `type_id`) VALUES
(2, 'Cơm chiên', 'abc', 60, 'ngon', 1);
INSERT INTO `foods` (`food_id`, `food_name`, `image`, `price`, `descs`, `type_id`) VALUES
(3, 'Tép rang', 'abc', 40, 'ngon', 3);
INSERT INTO `foods` (`food_id`, `food_name`, `image`, `price`, `descs`, `type_id`) VALUES
(4, 'Thịt kho', 'abc', 30, 'ngon', 3),
(5, 'Canh chua', 'abc', 20, 'ngon', 2),
(6, 'Canh cải', 'abc', 10, 'ngon', 2);

INSERT INTO `like_res` (`id_like`, `data_like`, `user_id`, `res_id`) VALUES
(1, '2023-09-09 00:00:00', 0, 2);
INSERT INTO `like_res` (`id_like`, `data_like`, `user_id`, `res_id`) VALUES
(2, '2023-09-12 00:00:00', 0, 1);
INSERT INTO `like_res` (`id_like`, `data_like`, `user_id`, `res_id`) VALUES
(3, '2023-09-15 00:00:00', 1, 2);
INSERT INTO `like_res` (`id_like`, `data_like`, `user_id`, `res_id`) VALUES
(4, '2023-09-21 00:00:00', 1, 3),
(5, '2023-09-11 00:00:00', 1, 1),
(6, '2023-09-05 00:00:00', 1, 1),
(7, '2023-09-09 00:00:00', 2, 2),
(8, '2023-09-12 00:00:00', 2, 1),
(9, '2023-09-15 00:00:00', 2, 2),
(10, '2023-09-21 00:00:00', 3, 3),
(11, '2023-09-11 00:00:00', 3, 1),
(12, '2023-09-05 00:00:00', 3, 1),
(13, '2023-09-17 00:00:00', 4, 3),
(14, '2023-09-11 00:00:00', 4, 1),
(15, '2023-09-05 00:00:00', 4, 1),
(16, '2023-09-25 00:00:00', 5, 3),
(17, '2023-09-13 00:00:00', 5, 1),
(18, '2023-09-01 00:00:00', 5, 1),
(19, '2023-09-22 00:00:00', 6, 3),
(20, '2023-09-19 00:00:00', 6, 1),
(21, '2023-09-02 00:00:00', 6, 1),
(22, '2023-09-17 00:00:00', 6, 3);

INSERT INTO `orders` (`id_orders`, `code`, `arr_sub_id`, `amount`, `user_id`, `food_id`) VALUES
(1, '1', 'B1', 115, 0, 1);
INSERT INTO `orders` (`id_orders`, `code`, `arr_sub_id`, `amount`, `user_id`, `food_id`) VALUES
(2, '2', 'B2', 75, 1, 2);
INSERT INTO `orders` (`id_orders`, `code`, `arr_sub_id`, `amount`, `user_id`, `food_id`) VALUES
(3, '3', 'B3', 50, 2, 3);
INSERT INTO `orders` (`id_orders`, `code`, `arr_sub_id`, `amount`, `user_id`, `food_id`) VALUES
(4, '4', 'B4', 40, 3, 4),
(5, '5', 'B5', 40, 4, 5),
(6, '6', 'B6', 26, 5, 6),
(7, '7', 'B7', 115, 0, 1),
(8, '8', 'B8', 75, 1, 2),
(9, '9', 'B9', 50, 1, 3),
(10, '10', 'B10', 40, 2, 4),
(11, '11', 'B11', 40, 3, 5);

INSERT INTO `rate_res` (`id_rate`, `amount`, `data_rate`, `user_id`, `res_id`) VALUES
(1, 1, '2023-09-24 00:00:00', 0, 1);
INSERT INTO `rate_res` (`id_rate`, `amount`, `data_rate`, `user_id`, `res_id`) VALUES
(2, 1, '2023-09-25 00:00:00', 1, 2);
INSERT INTO `rate_res` (`id_rate`, `amount`, `data_rate`, `user_id`, `res_id`) VALUES
(3, 1, '2023-09-20 00:00:00', 2, 1);
INSERT INTO `rate_res` (`id_rate`, `amount`, `data_rate`, `user_id`, `res_id`) VALUES
(4, 1, '2023-09-11 00:00:00', 3, 3),
(5, 1, '2023-09-01 00:00:00', 4, 1),
(6, 1, '2023-09-24 00:00:00', 0, 2),
(7, 1, '2023-09-22 00:00:00', 0, 1),
(8, 1, '2023-09-27 00:00:00', 1, 2),
(9, 1, '2023-09-02 00:00:00', 2, 1),
(10, 1, '2023-09-12 00:00:00', 3, 3),
(11, 1, '2023-09-16 00:00:00', 5, 1),
(12, 1, '2023-09-23 00:00:00', 4, 2),
(13, 1, '2023-09-17 00:00:00', 6, 1),
(14, 1, '2023-09-25 00:00:00', 3, 2),
(15, 1, '2023-09-20 00:00:00', 6, 1);

INSERT INTO `restaurants` (`res_id`, `res_name`, `image`, `descs`) VALUES
(1, 'Nhà hàng Á Âu', 'abc', 'Ngon xuất sắc');
INSERT INTO `restaurants` (`res_id`, `res_name`, `image`, `descs`) VALUES
(2, 'Nhà hàng Bắc', 'abc', 'Ngon xuất sắc');
INSERT INTO `restaurants` (`res_id`, `res_name`, `image`, `descs`) VALUES
(3, 'Nhà hàng Nhật Bản', 'abc', 'Ngon xuất sắc');

INSERT INTO `sub_foods` (`sub_id`, `sub_name`, `sub_price`, `food_id`) VALUES
(1, 'Salad trộn', 15, 1);
INSERT INTO `sub_foods` (`sub_id`, `sub_name`, `sub_price`, `food_id`) VALUES
(2, 'Salad trộn', 15, 2);
INSERT INTO `sub_foods` (`sub_id`, `sub_name`, `sub_price`, `food_id`) VALUES
(3, 'Rau sống', 10, 3);
INSERT INTO `sub_foods` (`sub_id`, `sub_name`, `sub_price`, `food_id`) VALUES
(4, 'Rau trộn', 10, 4),
(5, 'Rau câu', 20, 5),
(6, 'Bánh pudding', 16, 6);

INSERT INTO `users` (`user_id`, `fullname`, `email`, `password`) VALUES
(0, 'Nguyen Van A ', 'abc@gmail.com', '1234');
INSERT INTO `users` (`user_id`, `fullname`, `email`, `password`) VALUES
(1, 'Nguyen Van B ', 'bcd@gmail.com', '1234');
INSERT INTO `users` (`user_id`, `fullname`, `email`, `password`) VALUES
(2, 'Nguyen Van C ', 'cde@gmail.com', '1234');
INSERT INTO `users` (`user_id`, `fullname`, `email`, `password`) VALUES
(3, 'Nguyen Van D ', 'def@gmail.com', '1234'),
(4, 'Nguyen Van E ', 'efj@gmail.com', '1234'),
(5, 'Nguyen Van F ', 'abc@gmail.com', '1234'),
(6, 'Nguyen Van H', 'bac@gmal.com', '1234'),
(7, 'Nguyen Van K', 'haha@gmal.com', '1234');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;