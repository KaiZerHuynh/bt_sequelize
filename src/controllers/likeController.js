import initModels from "../models/init-models.js";
import sequelize from "../models/connect.js";

const model = initModels(sequelize);

const createLikes = async (req, res) => {
  const { res_id, user_id, data_like } = req.query;
  let createLike = await model.like_res.create({ user_id, res_id, data_like });
  res.send(createLike);
};

const deleteLikes = async (req, res) => {
  const id_like = req.params.id_like;
  await model.like_res.destroy({ where: { id_like } });
  res.sendStatus(204);
};

const getLikes = async (req, res) => {
  let likes = await model.like_res.findAll();
  res.send(likes);
};

export { getLikes, deleteLikes, createLikes };
